export type AppRoutes = "home" | "characterSSR" | "character" | "quote" | "quoteParent";

export interface AppParameters {
  characterId: string | undefined;
}
