import { AppParameters, AppRoutes } from "./routeNames";
import { routes } from "./routes";

const pushRoute = (routeName: AppRoutes, params?: AppParameters) => {
  routes.Router.pushRoute(routeName, { ...params });
};

const replaceRoute = (routeName: AppRoutes, params?: AppParameters) => routes.Router.replaceRoute(routeName, { ...params });

export { pushRoute, replaceRoute };
