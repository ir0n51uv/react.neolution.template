import { AppRoutes } from "./routeNames";
import Routes from "next-routes";

export interface IRoute {
  // next-routes properties
  name: AppRoutes;
  pattern: string;
  page: string;

  // calculated next-routes
  regex?: RegExp;
}

/**
 * Creates a parameterised string of the given AppParameter property.
 * @param name Parameter name
 * @param isOptional Whether the parameter is optional.
 * @returns Parameterised name.

const param = (name: keyof AppParameters, isOptional = false): string => `:${name}${isOptional ? "?" : ""}`;
*/

const routeConfigs: IRoute[] = [
  // home
  { name: "home", pattern: "/", page: "/home" },
  { name: "characterSSR", pattern: "/character/characterSSR", page: "/character/characterSSR" },
  { name: "character", pattern: "/character/character", page: "/character/character" },
  { name: "quote", pattern: "/quotes/quote", page: "/quotes/quote" },
  { name: "quoteParent", pattern: "/quotes/quoteParent", page: "/quotes/quoteParent" },
];

const routes = new Routes();
routeConfigs.forEach((item) => {
  routes.add(item);
});
export { routes, routeConfigs };
