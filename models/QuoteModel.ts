export interface QuoteModel {
  quote_id: number;
  quote: string;
  author: string;
  series: string;
}
