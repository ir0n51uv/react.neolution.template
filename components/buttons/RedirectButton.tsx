import React from "react";
import styles from "styles/Home.module.css";

interface RedirectButtonProps {
  label: string;
  onClick: () => void;
}

const RedirectButton = (props: RedirectButtonProps) => {
  const { label, onClick } = props;
  return (
    <a onClick={onClick} className={styles.card}>
      <h2>{label} &rarr;</h2>
    </a>
  );
};

export default RedirectButton;
