import React from "react";
import { pushRoute } from "routing/routeHelper";
import RedirectButton from "components/buttons/RedirectButton";
import BasicTemplate from "styles/templates/basic/basic.template";

const Home = () => (
  <BasicTemplate pageName="Home">
    <RedirectButton label={"Random character with SSR"} onClick={() => pushRoute("characterSSR")} />
    <RedirectButton label={"Random character with InitialProps"} onClick={() => pushRoute("character")} />
    <RedirectButton label={"Random quote with SSR"} onClick={() => pushRoute("quote")} />
    <RedirectButton label={"Random quote with SSR and parent/child"} onClick={() => pushRoute("quoteParent")} />
  </BasicTemplate>
);

export default Home;
