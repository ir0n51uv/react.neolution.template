import React from "react";
import { QuoteModel } from "models/QuoteModel";

interface QuoteChildProps {
  quote: QuoteModel;
  reloadQuote: () => void;
}

const QuoteChild = (pageProps: QuoteChildProps) => {
  const { quote, reloadQuote } = pageProps;
  const quoteLabel = (quote: QuoteModel): JSX.Element => (
    <div>
      <h1>{quote.author}: </h1>
      <h3>{quote.quote}</h3>
    </div>
  );

  return (
    quote && (
      <div>
        {quoteLabel(quote)}
        <button onClick={reloadQuote}>Reload</button>
      </div>
    )
  );
};

export default QuoteChild;
