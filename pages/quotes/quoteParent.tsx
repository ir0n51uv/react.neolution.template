import React, { useState } from "react";
import { QuoteModel } from "models/QuoteModel";
import { PageProps } from "misc/misc";
import { getRandomQuote } from "../api/client";
import BasicTemplate from "styles/templates/basic/basic.template";
import RedirectButton from "components/buttons/RedirectButton";
import { pushRoute } from "routing/routeHelper";
import QuoteChild from "./quoteChild";

interface QuoteParentProps {
  quote: QuoteModel;
}

const QuoteParent = (pageProps: QuoteParentProps) => {
  const { quote } = pageProps;
  const [quoteStatus, setQuoteStatus] = useState(quote);

  const reloadQuote = async () => {
    const quote = await getRandomQuote();
    setQuoteStatus(quote);
  };

  return (
    <div style={{ textAlign: "center" }}>
      <BasicTemplate pageName="CharacterSSR">
        <RedirectButton label={"Go to Home"} onClick={() => pushRoute("home")} />
      </BasicTemplate>

      <QuoteChild quote={quoteStatus} reloadQuote={reloadQuote} />
    </div>
  );
};

export async function getServerSideProps(): Promise<PageProps<QuoteParentProps>> {
  const quote = await getRandomQuote();
  return {
    props: { quote },
  };
}

export default QuoteParent;
