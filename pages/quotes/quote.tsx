import React from "react";
import { QuoteModel } from "models/QuoteModel";
import { PageProps } from "misc/misc";
import { getRandomQuote } from "../api/client";
import BasicTemplate from "styles/templates/basic/basic.template";
import RedirectButton from "components/buttons/RedirectButton";
import { pushRoute } from "routing/routeHelper";

interface QuoteProps {
  quote: QuoteModel;
}

const Quote = (pageProps: QuoteProps) => {
  const { quote } = pageProps;

  const quoteLabel = (quote: QuoteModel) => `${quote.author}: ${quote.quote}`;

  return (
    <div style={{ textAlign: "center" }}>
      <BasicTemplate pageName="CharacterSSR">
        <RedirectButton label={"Go to Home"} onClick={() => pushRoute("home")} />
        <RedirectButton label={"Try again"} onClick={() => pushRoute("quote")} />
      </BasicTemplate>
      {quote && (
        <div>
          <h2>{quoteLabel(quote)}</h2>
        </div>
      )}
    </div>
  );
};

export async function getServerSideProps(): Promise<PageProps<QuoteProps>> {
  const quote = await getRandomQuote();
  return {
    props: { quote },
  };
}

export default Quote;
