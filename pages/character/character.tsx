import React from "react";
import BasicTemplate from "styles/templates/basic/basic.template";
import RedirectButton from "components/buttons/RedirectButton";
import { pushRoute } from "routing/routeHelper";
import { CharacterModel } from "models/CharacterModel";
import { getRandomCharacter } from "../api/client";

interface CharacterProps {
  breakingBadCharacter: CharacterModel;
}

const Character = (props: CharacterProps) => {
  const { breakingBadCharacter } = props;
  return (
    <div style={{ textAlign: "center" }}>
      <BasicTemplate pageName="CharacterSSR">
        <RedirectButton label={"Go to Home"} onClick={() => pushRoute("home")} />
      </BasicTemplate>
      {breakingBadCharacter && (
        <div>
          <img src={breakingBadCharacter.img} alt="" />
          <h2>{breakingBadCharacter.name}</h2>
          <h2>{breakingBadCharacter.nickname}</h2>
        </div>
      )}
    </div>
  );
};

Character.getInitialProps = async (): Promise<CharacterProps> => {
  const breakingBadCharacter = await getRandomCharacter();
  return {
    breakingBadCharacter,
  };
};

export default Character;
