import React from "react";
import BasicTemplate from "styles/templates/basic/basic.template";
import RedirectButton from "components/buttons/RedirectButton";
import { pushRoute } from "routing/routeHelper";
import { CharacterModel } from "models/CharacterModel";
import { getRandomCharacter } from "../api/client";
import { PageProps } from "misc/misc";

interface CharacterProps {
  breakingBadCharacter: CharacterModel;
}

const CharacterSSR = (props: CharacterProps) => {
  const { breakingBadCharacter } = props;
  return (
    <div style={{ textAlign: "center" }}>
      <BasicTemplate pageName="CharacterSSR">
        <RedirectButton label={"Go to Home"} onClick={() => pushRoute("home")} />
      </BasicTemplate>
      {breakingBadCharacter && (
        <div>
          <img src={breakingBadCharacter.img} alt="" />
          <h2>{breakingBadCharacter.name}</h2>
          <h2>{breakingBadCharacter.nickname}</h2>
        </div>
      )}
    </div>
  );
};

export async function getServerSideProps(): Promise<PageProps<CharacterProps>> {
  const breakingBadCharacter = await getRandomCharacter();
  return {
    props: { breakingBadCharacter },
  };
}

export default CharacterSSR;
