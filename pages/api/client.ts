import axios from "axios";
import { CharacterModel } from "models/CharacterModel";
import { QuoteModel } from "models/QuoteModel";

const baseUrl = "https://www.breakingbadapi.com/api/";

export const getRandomCharacter = async (): Promise<CharacterModel> => {
  const response = await axios.get(`${baseUrl}character/random`);
  return response.data[0];
};

export const getRandomQuote = async (): Promise<QuoteModel> => {
  const response = await axios.get(`${baseUrl}quote/random`);
  return response.data[0];
};
