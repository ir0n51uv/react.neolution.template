# React.Neolution.Template

## What's inside

- Reusable component and template samples
- API https://breakingbadapi.com/ with ```axios```
- Routing (neo-way)
- Eslint (neo)
- Prettier (neo)
- Usual data fetching (neo)
- SSR data fetching (neo-way)

## Getting started

- Install dependencies ```yarn``` or ```npm i```
- Run ```yarn dev``` or ```npm run dev```
